<?php
class Track extends CI_Model{
	
	function addTrack($track_data){
		print_r($track_data);
		$this->db->insert('tracks',$track_data);
		$track_id = $this->db->insert_id(); 
		return $track_id;
	}
	
	function getAllTracks(){
		$this->db->select()->from('tracks');
		$query = $this->db->get();
		return $query->result_array();
	}

	function removeTracks($track_id){
			$this->db->where('id',$track_id);
			$this->db->delete('tracks');
	}

	function updateTrack($track_id,$track_data){
		$conditions = array(
			'id' => $track_id
			);
		$this->db->where($conditions);
		$this->db->update('tracks', $track_data);
		if($this->db->affected_rows()){
			return true;
		} else {
			return false;
		}
	}

}
?>