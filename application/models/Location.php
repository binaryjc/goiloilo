<?php
class Location extends CI_Model{

	function getAllLocations(){
		$this->db->select()->from('locations');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getLocationByFilter($category,$search_string){
		$this->db->select()->from('locations');
		if (isset($category) && $category!="0"){
			$this->db->where('locations.category_id',$category);
		}
		if (isset($search_string) && $search_string != "0"){
			$this->db->like("locations.name",urldecode($search_string));
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	function getLocationByCategory($category){
		$this->db->select()->from('locations');
		if (isset($category) && $category!="0"){
			$this->db->where('locations.category_id',$category);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	function getLocationByName($query){
		$this->db->select()->from('locations');
		//$this->db->where('locations.category_id',$category);
		$this->db->like('name', $query);
		$query = $this->db->get();
		return $query->result_array();
	}

	function getAllCategories(){
		//$this->db->select('categories.*,count(id) as total')->from('categories');
		$string_query = "SELECT categories.*,count(locations.id) as total FROM `categories` left join locations on categories.id = locations.category_id group by categories.id";
		//$query = $this->db->get();
		$query = $this->db->query($string_query);
		return $query->result_array();
	}

	function addlocation($data){
		$query = $this->db->insert('locations',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
}
?>