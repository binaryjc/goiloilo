<!doctype html>
<html>
  <head>
    <title>leaflet-gpx demo</title>
    <link rel="stylesheet" href="<?=base_url()?>resources/css/leaflet.css" />
  	<script src="<?= base_url(); ?>resources/js/jquery-2.1.3.js"></script>
    <style type="text/css">
      #map { height: 100vh; }
    </style>
  </head>
  <body>

    <?php
    $success = $this->session->flashdata('success');
    $errors = $this->session->flashdata('errors');
    if ($success || $errors):
    ?>
        <div class="general-notification trans-dark">
            <div class="flexhorizontal">
                <div class="container">
                    <div class="row">
                        <?php 
                          
                            if ($success):
                          ?>
                                      <div class="general-notify-inner white-sand roundborder p15">
                                        <h4 class="text-success"><span class="pr5"><i class="fa fa-smile-o"></i></span> 
                                          <?= $success; ?></h4>
                                      </div>
                          <?php     
                            endif;
                            if ($errors):
                                foreach($errors as $error):
                                    if ($error != ""):
                            ?>          
                                                <div class="general-notify-inner white-sand roundborder p15">
                                                    <h4 class="text-danger"><span class="pr5"><i class="fa fa-frown-o"></i></span> 
                                                  <?= $error; ?></h4>
                                                </div>
                            <?php
                                    endif;
                                endforeach;
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
    endif;
    ?>
    <div id="map"></div>
    <script src="<?=base_url()?>resources/js/leaflet.js"></script>
    
    <script>
      var map = L.map('map').setView([10.700713385402713, 122.56364822387697], 15);
      //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
          maxZoom: 18,
          id: 'your.mapbox.project.id',
          accessToken: 'your.mapbox.public.access.token'
      }).addTo(map);


      var getAllTracks = function(){
          <?php
          $getAllTracks = base_url()."clients/getAllTracks/";
          ?>
          //spinnerLoad();
          $.getJSON("<?= $getAllTracks; ?>", function (data){
                var num_text = 0;
            $.each(data, function(id,entry_data) { 

              $("#tracks_feed").append(
                '<div id="feed_track_num_'+entry_data['id']+'" class="gpx" data-gpx-source="<?=base_url()?>resources/gpx/'+entry_data['gpx_file']+'" data-map-target="targeted-map-'+entry_data['id']+'">'+
                  '<header>'+
                  '<div class="track_desc">'+entry_data['desc']+'</div>'+
                  '<div>'+entry_data['title']+'</div>'+
                  '<span class="start">'+entry_data['start_datetime']+'</span>'+
                  '</header>'+

                  '<article>'+
                    '<div id="map-container-feed">'+
                      '<div class="map" id="targeted-map-'+entry_data['id']+'"></div>'+
                    '</div>'+
                  '</article>'+

                   '<footer>'+
                      '<ul class="info">'+
                        '<li>Distance:&nbsp;<span class="distance">'+entry_data['distance']+'</span>&nbsp;mi</li>'+
                        '&mdash; <li>Duration:&nbsp;<span class="duration">'+entry_data['duration']+'</span></li>'+
                        '&mdash; <li>Pace:&nbsp;<span class="pace">'+entry_data['pace']+'</span>/mi</li>'+
                        '&mdash; <li>Avg&nbsp;HR:&nbsp;<span class="avghr">'+entry_data['avg_hour']+'</span>&nbsp;bpm</li>'+
                        '&mdash; <li>Elevation:&nbsp;+<span class="elevation-gain">'+entry_data['elevation_gain']+'</span>&nbsp;ft,'+
                          '-<span class="elevation-loss">'+entry_data['elevation_loss']+'</span>&nbsp;ft'+
                          '(net:&nbsp;<span class="elevation-net">'+entry_data['elevation_net']+'</span>&nbsp;ft)</li>'+
                      '</ul>'+
                    '</footer>'+
                '</div>'); 
                display_gpx(document.getElementById('feed_track_num_'+entry_data['id']));  
            });
          });
      }


      $(document).ready(function(){
        getAllTracks();
      });

    </script>
  </body>
</html>
