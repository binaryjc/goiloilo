<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8 />
    <title>Leaflet Locate</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='<?=base_url()?>resources/js/mapbox.js'></script>
    <!--<script src='<?=base_url()?>resources/js/zoom.js'></script>-->
    <link href='<?=base_url()?>resources/css/mapbox.css' rel='stylesheet' />
    <!-- <link href='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css' rel='stylesheet' /> -->
    <script src="<?= base_url(); ?>resources/js/jquery-2.1.3.js"></script>

    <link rel="stylesheet" href="<?=base_url()?>resources/full-scren-pushing-navigation/css/style.css" />
    <!--<link rel="stylesheet" href="<?=base_url()?>resources/css/zoom.css" />-->
    <link rel="stylesheet" href="<?=base_url()?>resources/css/bootstrap.min.css" />

    <style>
        body { margin:0; padding:0; }
        #map {position:absolute;top:0;left:0;width:100%;height:100%;}
        .fa-map-marker::before {
            font-size: 18px;
            padding: 8px;
            top: 3px;
            position: relative;
        }
        .pop_banner_image{width:280px;}
    </style>
</head>
<body>

    <script src='<?=base_url()?>resources/js/mapbox-locate.min.js'></script>
    <script src='<?=base_url()?>resources/js/bootstrap.min.js'></script>
    <link href='<?=base_url()?>resources/css/mapbox-locate.css' rel='stylesheet' /> 
    <!--<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.mapbox.css' rel='stylesheet' />-->
    <!--[if lt IE 9]>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.ie.css' rel='stylesheet' />
    <![endif]-->
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/css/font-awesome.min.css' rel='stylesheet' />

    <div id='map'></div>

    <nav style="position: absolute; top: 100px; left: 30px; color: rgb(255, 255, 255);">
        <ul class="" style="list-style:none;background: rgba(36, 48, 64, 0.5) none repeat scroll 0% 0%;padding:10px;">
        <?php foreach($categories as $row){?>
            <?php 
                $category_link = strtolower($row['name']).'_link';
            ?>
            <li>
                <input type="checkbox" checked name="<?=$category_link?>" id="<?=$category_link?>">
                <label for="<?=$category_link?>"><?=$row['name']?>(<?=$row['total']?>)</label>
            </li>
        <?php } ?>
        </ul>
    </nav>

    <a href="#cd-nav" class="cd-nav-trigger">Menu 
        <span class="cd-nav-icon"></span>
        <svg x="0px" y="0px" width="54px" height="54px" viewBox="0 0 54 54">
            <circle fill="transparent" stroke="#656e79" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
        </svg>
    </a>
    
    <div id="cd-nav" class="cd-nav">
        <div class="cd-navigation-wrapper">
            <div class="cd-half-block">
                <h2>Go-Iloilo</h2>
                <input type="text" name="search_query" id="search_query" class="">
                <ul id="search_results"></ul>
            </div><!-- .cd-half-block -->
            
            <div class="cd-half-block">
                <address>
                    <!--<nav>
                        <ul class="cd-primary-nav">
                            <! <li><input type="text" class="form-control" name="filter_string" id="filter_string" placeholder="Search Location By Name"></li>
                            <li>
                                <?php foreach($categories as $row){?>
                                    <?php 
                                        $category_link = strtolower($row['name']).'_link';
                                    ?>
                                    <a href="#" id="<?=$category_link?>"><?=$row['name']?></a>
                                <?php } ?>
                            </li>
                        </ul>
                    </nav> -->
                    <ul class="cd-contact-info">
                        <li><a href="mailto:info@myemail.co">info@myemail.co</a></li>
                        <li>0244-12345678</li>
                        <li>
                            <span>MyStreetName 24</span>
                            <span>W1234X</span>
                            <span>London, UK</span>
                        </li>
                    </ul>
                </address>
            </div> <!-- .cd-half-block -->
        </div> <!-- .cd-navigation-wrapper -->
    </div> <!-- .cd-nav -->
    <input type="hidden" id="locate_self_lat" value="">
    <input type="hidden" id="locate_self_lng" value="">
<script>
    //CUSTOMMARKER CREATOR
    var MyCustomMarker = L.Marker.extend({
                                bindPopup: function(htmlContent, options) {
                                    if (options && options.showOnMouseOver) {
                                        L.Marker.prototype.bindPopup.apply(this, [htmlContent, options]);
                                        this.off("click", this.openPopup, this);
                                        this.on("mouseover", function(e) {
                                            var target = e.originalEvent.fromElement || e.originalEvent.relatedTarget;
                                            var parent = this._getParent(target, "leaflet-popup");
                                            if (parent == this._popup._container)
                                                return true;
                                            this.openPopup();
                                        }, this);
                                        this.on("mouseout", function(e) {
                                            var target = e.originalEvent.toElement || e.originalEvent.relatedTarget;
                                            if (this._getParent(target, "leaflet-popup")) {
                                                L.DomEvent.on(this._popup._container, "mouseout", this._popupMouseOut, this);
                                                return true;
                                            }
                                            this.closePopup();
                                        }, this);
                                    }
                                },
                                _popupMouseOut: function(e) {
                                    L.DomEvent.off(this._popup, "mouseout", this._popupMouseOut, this);
                                    var target = e.toElement || e.relatedTarget;
                                    if (this._getParent(target, "leaflet-popup"))
                                        return true;
                                    if (target == this._icon)
                                        return true;
                                    this.closePopup();
                                },
                                _getParent: function(element, className) {
                                    if (element.parentNode) {
                                        parent = element.parentNode;
                                    }
                                    while (parent != null) {
                                        if (parent.className && L.DomUtil.hasClass(parent, className))
                                            return parent;
                                        parent = parent.parentNode;
                                    }
                                    return false;
                                }
    });
    //END CUSTOM MARKER

    customMarker = L.Marker.extend({
       options: { 
          category: 'Custom data!',
       }
    });


    //L.mapbox.accessToken = 'pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw';
    //var map = L.mapbox.map('map', 'mapbox.emerald').setView([10.700713385402713, 122.56364822387697], 15);
    //L.control.locate().addTo(map);

      var map = L.map('map',{ zoomControl:false }).setView([10.722345344678637,122.5605583190918], 13);
      //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a> | <a style="color:green;" href="https://www.facebook.com/binarywebdev/">binaryjc</a>',
          maxZoom: 18,
          id: 'mapbox.emerald',
          accessToken: 'pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw'
      }).addTo(map);
    new L.Control.Zoom({ position: 'topright' }).addTo(map);
    
    /*coordinate setter
    var coordinates = document.getElementById('coordinates');
    var marker2 = L.marker([10.700713385402713, 122.56364822387697], {
        draggable: true
    }).addTo(map);
    marker2.on('dragend', ondragend);
    ondragend();
    function ondragend() {
        var m = marker2.getLatLng();
        coordinates.innerHTML = 'Latitude: ' + m.lat + '<br />Longitude: ' + m.lng;
    }*/

    //ICONS
        var airportIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/airport.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'airport-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });

        var churchIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/church.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'church-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });

        var terminalIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/bus.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'terminal-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var ferryIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/ferry.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'ferry-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var firemanIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/fireman.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'fireman-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var foodIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/food.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'food-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var hospitalIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/hospital.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'school-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var hotelIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/hotel.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'hotel-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var landmarkIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/landmark.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'landmark-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var mallIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/mall.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'mall-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var marketIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/market.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'market-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var parkIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/park.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'park-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var policemanIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/policeman.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'policeman-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });

        var schoolIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/school.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'school-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });

        var govIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/gov.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'gov-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });

    //END ICONS

    //LAYER ARRAY GROUPS - Pre Defined

        var churches_layer_array = [];
        var school_layer_array = [];
        var hospital_layer_array = [];

    var getAllLocations = function(){
        <?php
        $getAllLocations = base_url()."mapbox/getAllLocations/";
        ?>

        $.getJSON("<?= $getAllLocations; ?>", function (data){  
            $.each(data, function(id,entry_data){ 
                console.log(entry_data['name']);
                console.log(entry_data['latlng']);
                console.log(entry_data['content']);
                var iconResulted = entry_data['icon'];
                var coordinatesResulted = entry_data['latlng'];
                var res = coordinatesResulted.split(",");
                var lat = parseFloat(res[0], 10);
                var lng = parseFloat(res[1], 10);
                var latlng = L.latLng(lat, lng);
                    var banner_link = "<?=base_url()?>resources/images/location_images/"+entry_data['banner'];
                    var banner_img = '<img class="pop_banner_image" src="'+banner_link+'">';
                var content = banner_img+'<br/>'+entry_data['content'];
                var name = entry_data['name'];

                if (iconResulted == "church.png") {
                    iconChoosen = churchIcon;
                };
                if (iconResulted == "bus.png") {
                    iconChoosen = terminalIcon;
                };
                if (iconResulted == "school.png") {
                    iconChoosen = schoolIcon;
                };
                if (iconResulted == "hospital.png") {
                    iconChoosen = hospitalIcon;
                };
                if (iconResulted == "gov.png") {
                    iconChoosen = govIcon;
                };

                /*hover marker
                   var marky = new MyCustomMarker(latlng, {
                        icon: iconChoosen
                    }).addTo(map);
                    marky.bindPopup(content, {
                        showOnMouseOver: true
                    });
                */

                var marky = L.marker(latlng, {icon: iconChoosen}).addTo(map).bindPopup(content);


                if (entry_data['category_id'] == 1 ) {
                    churches_layer_array.push(marky);
                };
                if (entry_data['category_id'] == 10 ) {
                    school_layer_array.push(marky);
                };
                if (entry_data['category_id'] == 7 ) {
                    hospital_layer_array.push(marky);
                };

            });

            //CREATE LAYER GROUPS NOW
                var churches_layer = L.layerGroup(churches_layer_array).addTo(map);
                var school_layer = L.layerGroup(school_layer_array).addTo(map);
                var hospital_layer = L.layerGroup(hospital_layer_array).addTo(map);

            //LAYER GROUP FILTER BUTTON ACTIONS
                document.getElementById("church_link").onclick = function() {
                    if (map.hasLayer(churches_layer)) {
                        map.removeLayer(churches_layer);
                    } else {
                        map.addLayer(churches_layer);
                    }
                }
                document.getElementById("school_link").onclick = function() {
                    if (map.hasLayer(school_layer)) {
                        map.removeLayer(school_layer);
                    } else {
                        map.addLayer(school_layer);
                    }
                }
                document.getElementById("hospital_link").onclick = function() {
                    if (map.hasLayer(hospital_layer)) {
                        map.removeLayer(hospital_layer);
                    } else {
                        map.addLayer(hospital_layer);
                    }
                    //map.addLayer(sponsors_layer);
                }

        });
    }



    var getLocationByFilter = function(category,string_search){
        <?php
        $getLocationByFilter = base_url()."mapbox/getLocationByFilter/";
        ?>

        //INSERT CLEAR MAP COMMAND
        
        $.getJSON("<?= $getLocationByFilter; ?>"+category+"/"+string_search, function (data){  
            $.each(data, function(id,entry_data){ 
            console.log(entry_data['name']);
            console.log(entry_data['latlng']);
            console.log(entry_data['content']);
            var iconResulted = entry_data['icon'];
            var coordinatesResulted = entry_data['latlng'];
            var res = coordinatesResulted.split(",");
            var lat = parseFloat(res[0], 10);
            var lng = parseFloat(res[1], 10);
            var latlng = L.latLng(lat, lng);
            var content = entry_data['content'];
            var name = entry_data['name'];

            if (iconResulted == "church.png") {
                iconChoosen = churchIcon;
            };
            if (iconResulted == "bus.png") {
                iconChoosen = terminalIcon;
            };
            if (iconResulted == "school.png") {
                iconChoosen = schoolIcon;
            };
            if (iconResulted == "hospital.png") {
                iconChoosen = hospitalIcon;
            };

               var marky = new MyCustomMarker(latlng, {
                    icon: iconChoosen
                }).addTo(map);
                marky.bindPopup(content, {
                    showOnMouseOver: true
                });

            });
            
        });

    }

    $(document).ready(function(){
        getAllLocations();
    });


    //SEARCHING
    $( "#search_query" ).keyup(function(e) {

    if($( "#search_query").val() == 0){
       $( "#search_results" ).empty();
    }

    if(e.which == 13) {
       $( "#search_results" ).empty();
       searchLocation($( "#search_query").val());
    }

    });

    function searchLocation(search_text){
        <?php
        $searchLocation = base_url()."mapbox/searchLocation/";
        ?>
        console.log("<?= $searchLocation; ?>"+search_text);
        $.getJSON("<?= $searchLocation; ?>"+search_text, function (data){  
            $( "#search_results").empty();
            $.each(data, function(id,entry_data){
                var coordinatesResulted = entry_data['latlng'];
                var res = coordinatesResulted.split(",");
                var lat = parseFloat(res[0], 10);
                var lng = parseFloat(res[1], 10);
                var latlng = L.latLng(lat, lng);
                $("#search_results").append(
                    //function out to add effect
                    '<li class="search-result-entry"><a href="#" onclick="map.panTo(['+res+']);">'+entry_data['name']+'</a></li>'
                    );
            });
        });
    }


</script>
<script src="<?= base_url(); ?>resources/full-scren-pushing-navigation/js/main.js"></script>

</body>
</html>

