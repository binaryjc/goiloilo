<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8 />
    <title>Admin Dashboard</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='<?=base_url()?>resources/js/mapbox.js'></script>
    <script src='<?=base_url()?>resources/js/zoom.js'></script>
    <link href='<?=base_url()?>resources/css/mapbox.css' rel='stylesheet' />
    <!-- <link href='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css' rel='stylesheet' /> -->
    <script src="<?= base_url(); ?>resources/js/jquery-2.1.3.js"></script>

    <link rel="stylesheet" href="<?=base_url()?>resources/full-scren-pushing-navigation/css/style.css" />
    <link rel="stylesheet" href="<?=base_url()?>resources/css/zoom.css" />
    <link rel="stylesheet" href="<?=base_url()?>resources/css/bootstrap.min.css" />

</head>
<body>	
    <script src='<?=base_url()?>resources/js/mapbox-locate.min.js'></script>
    <script src='<?=base_url()?>resources/js/bootstrap.min.js'></script>
    <link href='<?=base_url()?>resources/css/mapbox-locate.css' rel='stylesheet' /> 
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h1>
						Admin Dashboard <small>Add/Edit/Delete Locations</small>
					</h1>
				</div>

				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Latitude-Longitude</th>
									<th>Icon</th>
									<th>Category</th>
									<th>Content</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="locations_table">
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<h3>
							
						</h3>
						<div>
							<a href="#panel-element-14258" class="btn btn-default btn-sm btn-left" data-toggle="collapse"><span class="pr5"><i class="fa fa-plus-circle text-primary"></i></span>Add New Location</a>	
						</div>
						<br/>

					<!--ERROR PANEL -->
						<?php
						$success = $this->session->flashdata('success');
						$errors = $this->session->flashdata('errors');
						if ($success || $errors):
						?>
						    <div class="general-notification trans-dark">
						        <div class="flexhorizontal">
						            <div class="container">
						                <div class="row">
						                    <?php 
						                        if ($success):
						                      ?>
						                                  <div class="general-notify-inner white-sand roundborder p15">
						                                    <h4 class="text-success"><span class="pr5"><i class="fa fa-smile-o"></i></span> 
						                                      <?= $success; ?></h4>
						                                  </div>
						                      <?php     
						                        endif;
						                        if ($errors):
						                            foreach($errors as $error):
						                                if ($error != ""):
						                        ?>
						                                            <div class="general-notify-inner white-sand roundborder p15">
						                                                <h4 class="text-danger"><span class="pr5"><i class="fa fa-frown-o"></i></span> 
						                                              <?= $error; ?></h4>
						                                            </div>
						                        <?php
						                                endif;
						                            endforeach;
						                        endif;
						                    ?>
						                </div>
						            </div>
						        </div>
						    </div>
						<?php
						endif;
						?>
					<!--END ERROR PANEL -->
						<div class="panel-group" id="panel-121069">
							<div class="panel panel-default">
								<div id="panel-element-14258" class="panel-collapse collapse in">
									<div class="panel-body">
										
			    					<?php
									$attributes = array('role' => 'form');
									echo form_open_multipart('admins/add_location');
									?>
											<div class="form-group">
												 
												<label for="loc_name">
													Name
												</label>
												<input class="form-control" id="loc_name" name="loc_name" required type="text" />
											</div>
											<div class="form-group">
												<label for="loc_latlng">
													Latitude-Longitude
												</label>
												<input class="form-control" id="loc_latlng" name="loc_latlng" required type="text" />
												<p class="help-block">
													example: 10.723457472354175,122.55618631839751
												</p>
												<div id='map' style="height:50vh;width:100%;"></div> 
											</div>
											<div class="form-group">
												<label for="loc_icon">
													Icon
												</label>
												<select class="form-control" id="loc_icon" name="loc_icon" required>
													<option selected disabled >Select Icon</option>
													<option value="church.png">Church</option>
													<option value="landmark.png">Landmark</option>
													<option value="gov.png">Government</option>
												</select>
											</div>
											<div class="form-group">
												<label for="loc_cat">
													Category
												</label>
												<select class="form-control" id="loc_cat" name="loc_cat" required >
													<option selected disabled >Select Category</option>
													<?php foreach($categories as $row){?>
														<option value="<?=$row['id']?>"><?=$row['name']?></option>
													<?php } ?>
												</select>
											</div>

											<div class="form-group">
												<label for="loc_content">
													Content
												</label>
												<input type="hidden" name="loc_type" value="normal"/>
												<textarea class="form-control mce" id="loc_content" name="loc_content"  required></textarea>
											</div>

											<button type="submit" class="btn btn-default">
												Submit
											</button>
									<?= form_close(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>	
<script>
	
    var admin_getAllLocations = function(){
        <?php
        $getAllLocations = base_url()."mapbox/getAllLocations/";
        ?>

		$.getJSON("<?= $getAllLocations; ?>", function (data){
		    var total_queries = data.length;
		    $('#count_total_queries').text(total_queries); 
		        if(total_queries <= 0){
		          $("#locations_table").append('<tr>'+
		              '<td class="text-center" colspan="8">'+
		                'No Locations found.'+
		              '</td>'+
		            '</tr>');
		        }else{
		          var num_text = 0;
		          $.each(data, function(id,entry_data) { 
					num_text = num_text+1;

		            $("#locations_table").append('<tr>'+
		               '<td>'+num_text+'</td>'+
		                '<td class="text-center">'+entry_data['name']+'</td>'+
		                '<td class="text-center">'+entry_data['latlng']+'</td>'+
		                '<td class="text-center">'+entry_data['icon']+'</td>'+
		                '<td class="text-center">'+entry_data['category_id']+'</td>'+
		                '<td class="text-center">'+entry_data['content']+
		                	'<br><a href="#modal-container-view-location-content" onclick="getSpecificLocContent('+entry_data['id']+');" role="button" class="" data-toggle="modal">View Full Content</a>'+
		                '</td>'+
		                '<td class="text-center">'+'<a href="#modal-container-edit-location" onclick="getSpecificLocation('+entry_data['id']+');" role="button" class="" data-toggle="modal">Edit</a>'+'</td>'+
		            '</tr>');   
		          });
		        }
		});


    }

    $(document).ready(function(){
        admin_getAllLocations();
    });


	var map = L.map('map').setView([10.700713385402713, 122.56364822387697], 16);
      //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
          maxZoom: 20,
          id: 'mapbox.emerald',
          accessToken: 'pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw'
      }).addTo(map);

    //coordinate setter
    var coordinates_input = document.getElementById('loc_latlng');
    var marker2 = L.marker([10.700713385402713, 122.56364822387697], {
        draggable: true
    }).addTo(map);
    marker2.on('dragend', ondragend);
    ondragend();
    function ondragend() {
        var m = marker2.getLatLng();
        coordinates_input.value = m.lat+","+m.lng;
    }


</script>
</body>
</html>
