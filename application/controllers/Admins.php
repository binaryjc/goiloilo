<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends CI_Controller {

	public function index(){
		$this->load->model('location');
		
		$data['categories'] = $this->location->getAllCategories();
		$this->load->view('admin_dashboard',$data);
	}

	public function add_location(){

		$this->load->model('location');
		if ($_POST){
			//Add 
						$data = array(
									'name' => $_POST['loc_name'], 
									'latlng' => $_POST['loc_latlng'], 
									'icon' => $_POST['loc_icon'], 
									'category_id' => $_POST['loc_cat'], 
									'type' => $_POST['loc_type'], 
									'content' => $_POST['loc_content']
								);
						$result = $this->location->addlocation($data);

					if ($result == true){
						$this->session->set_flashdata('success', 'Location was successfully added.');
						redirect("admins/");
					} else {
						$this->session->set_flashdata('errors', $result['errors']);
						redirect("admins/");
					}
		}
	}

	public function edit_location(){

	}

	public function delete_location(){

	}



}