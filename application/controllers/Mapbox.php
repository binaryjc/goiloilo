<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapbox extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$this->load->model('location');
		$data['categories'] = $this->location->getAllCategories();
		$this->load->view('mapbox',$data);
	}

	public function getAllLocations(){
		$this->load->model('location');
		$locations = $this->location->getAllLocations();
			//echo '<pre>';
			//print_r($locations);
			//die();
		echo json_encode($locations);

	}
	public function getLocationByFilter($category,$search_string){
		$this->load->model('location');
		$locations = $this->location->getLocationByFilter($category,$search_string);
			//echo '<pre>';
			//print_r($locations);
			//die();
		echo json_encode($locations);

	}
	public function getLocationByCategory($category){
		$this->load->model('location');
		$locations = $this->location->getLocationByCategory($category,$search_string);
			//echo '<pre>';
			//print_r($locations);
			//die();
		echo json_encode($locations);

	}

	public function searchLocation($query){
		$this->load->model('location');
		$locations = $this->location->getLocationByName($query);
			//echo '<pre>';
			//print_r($locations);
			//die();
		echo json_encode($locations);

	}

}
