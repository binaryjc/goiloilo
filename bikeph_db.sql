-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2016 at 02:04 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bikeph_db`
--
CREATE DATABASE IF NOT EXISTS `bikeph_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bikeph_db`;

-- --------------------------------------------------------

--
-- Table structure for table `tracks`
--

CREATE TABLE IF NOT EXISTS `tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(350) NOT NULL,
  `desc` varchar(500) NOT NULL,
  `start_datetime` varchar(50) NOT NULL,
  `distance` int(11) NOT NULL,
  `duration` time NOT NULL,
  `pace` time NOT NULL,
  `avg_hour` int(11) NOT NULL,
  `elevation_gain` int(11) NOT NULL,
  `elevation_loss` int(11) NOT NULL,
  `elevation_net` int(11) NOT NULL,
  `pics` varchar(150) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gpx_file` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(50) NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(35) NOT NULL,
  `title` varchar(70) DEFAULT NULL,
  `filename` varchar(75) DEFAULT 'default.jpg',
  `birthdate` date DEFAULT NULL,
  `about_me` text,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `google_plus` varchar(100) DEFAULT NULL,
  `linked_in` varchar(100) DEFAULT NULL,
  `address` varchar(130) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `user_type` enum('client','admin') NOT NULL,
  `client_type_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `is_online` tinyint(1) NOT NULL DEFAULT '0',
  `is_activated` varchar(32) NOT NULL DEFAULT '1',
  `last_activity` datetime DEFAULT NULL,
  `date_registered` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
