-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2016 at 06:55 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `goiloilo`
--
CREATE DATABASE IF NOT EXISTS `goiloilo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `goiloilo`;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `icon`) VALUES
(1, 'Church', ''),
(2, 'Terminal', ''),
(3, 'Police Station', ''),
(4, 'Fire Station', ''),
(5, 'Landmark', ''),
(6, 'Food & Drinks', ''),
(7, 'Hospital', ''),
(8, 'Park', ''),
(9, 'Hotel', ''),
(10, 'School', ''),
(11, 'Market', ''),
(12, 'Mall', ''),
(13, 'Airport', ''),
(14, 'Ferry', '');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `latlng` text NOT NULL,
  `icon` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `latlng`, `icon`, `content`, `type`, `category_id`) VALUES
(1, 'Jaro Cathedral', '10.723457472354175,122.55618631839751', 'church.png', 'Jaro Cathedral', 'normal', 1),
(2, 'WVSU', '10.713595761651844,122.56251096725462', 'school.png', 'West Visayas State University', 'normal', 10),
(3, 'St. Pauls Hospital', '10.702189299162592,122.56669521331786', 'hospital.png', 'St. Pauls Hospital', 'normal', 7),
(4, 'TEST', '10.734067281027063,122.58613586425781', 'church.png', 'test', 'normal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
